"use strict";
exports.__esModule = true;
/**
 * class defining the linkedlist data structure
 *
 * @export
 * @class LinkedList
 */
var LinkedList = /** @class */ (function () {
    function LinkedList() {
        // Setting the linked lists head and tail pointers to null initializes an empty linked list.
        this.head = null;
        this.tail = null;
    }
    /**
     * Add a node to the front of the linked list
     *
     * @param {Node} node
     * @memberof LinkedList
     */
    LinkedList.prototype.addToStart = function (node) {
        // Save the old head node, so it can be used later on
        var oldHeadNode = this.head;
        // Point the head to the new node
        this.head = node;
        this.head.next = oldHeadNode;
        LinkedList.count++;
        // If the list was empty the tail should also point to the new node
        if (LinkedList.count === 1) {
            this.tail = node;
        }
    };
    /**
     * Add a node to the end of the linked list
     *
     * @param {Node} node
     * @memberof LinkedList
     */
    LinkedList.prototype.addToEnd = function (node) {
        // If its an empty linked list
        if (LinkedList.count === 0) {
            this.head = node;
        }
        else {
            var oldTailNode = this.tail;
            oldTailNode.next = node;
        }
        LinkedList.count++;
        this.tail = node;
    };
    /**
     * Remove the last node in the linked list.
     *
     * @memberof LinkedList
     */
    LinkedList.prototype.removeLast = function () {
        // Ensure that the linked list has nodes in it before trying to remove any node.
        if (LinkedList.count != 0) {
            // For linked lists with only 1 node, remove that node by setting head and tail to null
            if (LinkedList.count === 1) {
                this.head = null;
                this.tail = null;
            }
            else {
                var currentNode = this.head;
                // Traverse through the list until we reach the last node before the tail (because we are about to delete the tail)
                while (currentNode.next != this.tail) {
                    currentNode = currentNode.next;
                }
                this.tail = currentNode;
                this.tail.next = null;
            }
            LinkedList.count--;
        }
    };
    /**
     * Remove the first node from a linked list
     *
     * @memberof LinkedList
     */
    LinkedList.prototype.removeFirst = function () {
        if (LinkedList.count != 0) {
            this.head = this.head.next;
            LinkedList.count--;
            if (LinkedList.count === 0) {
                this.tail = null;
            }
        }
    };
    LinkedList.count = 0;
    return LinkedList;
}());
exports["default"] = LinkedList;
