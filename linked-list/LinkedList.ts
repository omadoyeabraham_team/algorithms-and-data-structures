import { Node } from "../node/Node";

/**
 * class defining the linkedlist data structure
 *
 * @export
 * @class LinkedList
 */
export default class LinkedList {
  public head: Node;
  public tail: Node;
  public static count: number = 0;

  constructor() {
    // Setting the linked lists head and tail pointers to null initializes an empty linked list.
    this.head = null;
    this.tail = null;
  }

  /**
   * Add a node to the front of the linked list
   *
   * @param {Node} node
   * @memberof LinkedList
   */
  addToStart(node: Node) {
    // Save the old head node, so it can be used later on
    const oldHeadNode: Node = this.head;

    // Point the head to the new node
    this.head = node;

    this.head.next = oldHeadNode;
    LinkedList.count++;

    // If the list was empty the tail should also point to the new node
    if (LinkedList.count === 1) {
      this.tail = node;
    }
  }

  /**
   * Add a node to the end of the linked list
   *
   * @param {Node} node
   * @memberof LinkedList
   */
  addToEnd(node: Node) {
    // If its an empty linked list
    if (LinkedList.count === 0) {
      this.head = node;
    } else {
      const oldTailNode = this.tail;
      oldTailNode.next = node;
    }

    LinkedList.count++;
    this.tail = node;
  }

  /**
   * Remove the last node in the linked list.
   *
   * @memberof LinkedList
   */
  removeLast() {
    // Ensure that the linked list has nodes in it before trying to remove any node.
    if (LinkedList.count != 0) {
      // For linked lists with only 1 node, remove that node by setting head and tail to null
      if (LinkedList.count === 1) {
        this.head = null;
        this.tail = null;
      } else {
        let currentNode: Node = this.head;

        // Traverse through the list until we reach the last node before the tail (because we are about to delete the tail)
        while (currentNode.next != this.tail) {
          currentNode = currentNode.next;
        }

        this.tail = currentNode;
        this.tail.next = null;
      }

      LinkedList.count--;
    }
  }

  /**
   * Remove the first node from a linked list
   *
   * @memberof LinkedList
   */
  removeFirst() {
    if (LinkedList.count != 0) {
      this.head = this.head.next;
      LinkedList.count--;

      if (LinkedList.count === 0) {
        this.tail = null;
      }
    }
  }
}
