"use strict";
exports.__esModule = true;
var Node_1 = require("./node/Node");
var LinkedList_1 = require("./linked-list/LinkedList");
var enumerators_1 = require("./shared/enumerators");
function testNodes() {
    var first = new Node_1.Node(1);
    var second = new Node_1.Node(2);
    var third = new Node_1.Node(3);
    var fourth = new Node_1.Node(4);
    first.next = second;
    second.next = third;
    third.next = fourth;
    enumerators_1.traverseNodeChain(first, function (node) {
        console.log(node.value);
    });
}
function testLinkedLists() {
    var list = new LinkedList_1["default"]();
    var first = new Node_1.Node(1);
    var second = new Node_1.Node(2);
    var third = new Node_1.Node(3);
    var fourth = new Node_1.Node(4);
    list.addToStart(first);
    list.addToEnd(second);
    list.addToStart(third);
    list.removeLast();
    list.removeLast();
    list.removeLast();
    enumerators_1.traverseNodeChain(list, function (node) {
        console.log(node.value);
    });
}
testLinkedLists();
