import { Node } from "./node/Node";
import LinkedList from "./linked-list/LinkedList";
import { traverseNodeChain } from "./shared/enumerators";

function testNodes() {
  let first: Node = new Node(1);
  let second: Node = new Node(2);
  let third: Node = new Node(3);
  let fourth: Node = new Node(4);

  first.next = second;
  second.next = third;
  third.next = fourth;

  traverseNodeChain(first, node => {
    console.log(node.value);
  });
}

function testLinkedLists() {
  let list = new LinkedList();
  let first: Node = new Node(1);
  let second: Node = new Node(2);
  let third: Node = new Node(3);
  let fourth: Node = new Node(4);

  list.addToStart(first);
  list.addToEnd(second);
  list.addToStart(third);

  list.removeLast();
  list.removeLast();
  list.removeLast();

  traverseNodeChain(list, node => {
    console.log(node.value);
  });
}

testLinkedLists();
