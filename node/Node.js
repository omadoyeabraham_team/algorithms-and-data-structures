"use strict";
exports.__esModule = true;
/**
 * Definition for a Node class
 * Nodes are a simple data structure, utilised in many other data structures
 *
 * @export
 * @class Node
 */
var Node = /** @class */ (function () {
    function Node(value, next) {
        if (next === void 0) { next = null; }
        this.value = value;
        this.next = next;
    }
    return Node;
}());
exports.Node = Node;
