/**
 * Definition for a Node class
 * Nodes are a simple data structure, utilised in many other data structures
 *
 * @export
 * @class Node
 */
export class Node {
  public value: any;
  public next: Node;

  constructor(value, next = null) {
    this.value = value;
    this.next = next;
  }
}
