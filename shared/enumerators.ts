import { Node } from "../node/Node";
import LinkedList from "../linked-list/LinkedList";

/**
 * Enumerate over each node in a node chain and run the cb function
 * for each node in the node chain
 *
 * @export
 * @param {Node} node
 * @param {Function} cb
 */
export function traverseNodeChain(list: LinkedList, cb: Function) {
  // Ensure that a valid Node object is provided
  if (!list) {
    console.log("You did not provide a list");
    return;
  }

  if (!list.head) {
    console.log("You provided an empty list");
    return;
  }

  // Ensure that a function is passed in as the second argument
  if (typeof cb !== "function") {
    throw new Error(
      "The printNodeChain function expects a valid function as its second argument"
    );
  }

  let node = list.head;

  while (node) {
    cb(node);
    node = node.next;
  }
}
